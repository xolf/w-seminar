window.simulate = {
	mapContainer: $("#map"),
	map: null,
	accuracy: 0.001,
    positions: {
	    start: {
	        lat: 0,
            lon: 0
        },
        end: {
	        lat: 60,
            lon: 60
        }
    },
	
	bootstrapReady: function() {
		$("#loading").remove();
	},
	
	bootstrap: function() {
		this.mapContainer.css('height',window.innerHeight+'px');
		this.mapContainer.css('widht', '100%');
		//this.mapContainer.css('background-color', 'red');
		
		this.map = L.map('map').setView([51.505, -0.09], 13);
		L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
			attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
		}).addTo(this.map);
				
		this.bootstrapReady();
		
		//this.draw([51,0], [40,-73.9]);
        this.draw([this.positions.start.lat,this.positions.start.lon], [this.positions.end.lat, this.positions.end.lon]);
	},
	
	draw: function(startArr, endArr){

		var greatCircleCalculation = greatCircle.calculate(startArr, endArr);

		console.log(greatCircleCalculation);
		$('#greatCircleLength').text(greatCircleCalculation.length.km+'km');
        $('#vincentyLength').text(greatCircleCalculation.ellipsoid.distance.km+'km');
        var distanceDiff = Math.round(Math.abs(greatCircleCalculation.ellipsoid.distance.km - greatCircleCalculation.length.km) * 100) / 100;
        $('#differenceLength').text(distanceDiff+'km');

        L.marker(greatCircleCalculation.start).addTo(this.map)
            .bindPopup('Start')
            .openPopup();

        L.marker(greatCircleCalculation.end).addTo(this.map)
            .bindPopup('Ende')
            .openPopup();

        var coordsArr = [];
        coordsArr[coordsArr.length] = [greatCircleCalculation.start.lon, greatCircleCalculation.start.lat];
        var newCoords = greatCircle.getNextCoordinates(greatCircleCalculation.start, greatCircleCalculation.course.radians, this.accuracy);
        var oldLength = 99999999999999999999999;
        while(greatCircleCalculation.length.m < oldLength) {
			newCoords = greatCircle.getNextCoordinates(greatCircleCalculation.start, greatCircleCalculation.course.radians, this.accuracy);

            coordsArr[coordsArr.length] = [newCoords.lon, newCoords.lat];
            oldLength = greatCircleCalculation.length.m;
			greatCircleCalculation = greatCircle.calculate([newCoords.lat, newCoords.lon] , endArr)
		}
        coordsArr[coordsArr.length-1] = [greatCircleCalculation.end.lon, greatCircleCalculation.end.lat];

        $('#waypoints').text(coordsArr.length);

        this.leaflet.setView([(startArr[0]+endArr[0])/2, (startArr[1]+endArr[1])/2], 2);

        var line = [{
            "type": "LineString",
            "coordinates": coordsArr
        }];

        var lineStyle = {
            "color": "#5088bc",
            "weight": 2,
            "opacity": 1
        };
		
		L.geoJSON(line, {
			style: lineStyle
		}).addTo(this.map);
	},
	
	leaflet: {
		setView: function(coord, zoom) {
			simulate.map.setView(coord, zoom);
		}
	}
};
