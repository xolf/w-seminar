window.greatCircle = {

    // Erd-Radius in Meter
    earthRadius: 6378137,

    vincenty: function(φ1, λ1, φ2, λ2) {
        var a = 6378137; // in Meter
        var b = 6356752.3142; // in Meter
        var f = (a-b)/a;

        var L = λ2 - λ1;

        var tanU1 = (1-f) * Math.tan(φ1);
        var cosU1 = 1 / Math.sqrt((1 + tanU1*tanU1));
        var sinU1 = tanU1 * cosU1;

        var tanU2 = (1-f) * Math.tan(φ2);
        var cosU2 = 1 / Math.sqrt((1 + tanU2*tanU2));
        var sinU2 = tanU2 * cosU2;

        var λ = L;
        var λʹ = 0;
        var n = 250; // max. Anzahl an Iterationen

        do {
            var sinλ = Math.sin(λ), cosλ = Math.cos(λ);
            var sinSqσ = (cosU2*sinλ) * (cosU2*sinλ) + (cosU1*sinU2-sinU1*cosU2*cosλ) * (cosU1*sinU2-sinU1*cosU2*cosλ);
            var sinσ = Math.sqrt(sinSqσ);

            if (sinσ === 0) {
                // Die Punkte liegen aufeinander
                return {
                    distance: 0,
                    route: 0
                };
            }

            var cosσ = sinU1*sinU2 + cosU1*cosU2*cosλ;
            var σ = Math.atan2(sinσ, cosσ);

            var sinα = cosU1 * cosU2 * sinλ / sinσ;
            var cosSqα = 1 - sinα*sinα;

            var cos2σm = cosσ - 2*sinU1*sinU2/cosSqα;
            if (isNaN(cos2σm)) {
                // Wenn cosSqα=0 Punkte liegen auf der Äquatorial Linie
                cos2σm = 0;
            }

            var C = f/16*cosSqα*(4+f*(4-3*cosSqα));

            λʹ = λ;
            λ = L + (1-C) * f * sinα * (σ + C*sinσ*(cos2σm+C*cosσ*(-1+2*cos2σm*cos2σm)));

        } while (Math.abs(λ-λʹ) > 1e-12 && --n > 0);
        if (n === 0) {
            alert('Es konne kein Ergebniss gefunden werden!');
        }

        var uSq = cosSqα * (a*a - b*b) / (b*b);

        var A = 1 + uSq/16384*(4096+uSq*(-768+uSq*(320-175*uSq)));
        var B = uSq/1024 * (256+uSq*(-128+uSq*(74-47*uSq)));

        var Δσ = B * sinσ * (cos2σm + B/4 *(cosσ * (-1 + 2*cos2σm*cos2σm) - B/6 * cos2σm * (-3 + 4*sinσ*sinσ)* (-3 + 4*cos2σm*cos2σm)));

        var distance = b * A * (σ-Δσ);

        var route = Math.atan2(cosU2*sinλ,  cosU1*sinU2-sinU1*cosU2*cosλ);

        return {
            distance: distance,
            route: route
        }
    },

    greatCircleDistance: function (φ1, λ1, φ2, λ2) {
        var lamda = Math.acos( Math.sin(φ1) * Math.sin(φ2) + Math.cos(φ1) * Math.cos(φ2) * Math.cos(λ2-λ1) );
        var length = lamda * this.earthRadius;

        return length;
    },
    
    calculate: function(startArr, endArr){
        var start = {lat: startArr[0], lon: startArr[1]},
            end = {lat: endArr[0], lon: endArr[1]},
            startRad = {lat: start.lat.toRadians(), lon: start.lon.toRadians()},
            endRad = {lat: end.lat.toRadians(), lon: end.lon.toRadians()};

        // Breitengrad
        var φ1 = startRad.lat;
        var φ2 = endRad.lat;
        // Längengrad
        var λ1 = startRad.lon;
        var λ2 = endRad.lon;

        var greatCircleLength = this.greatCircleDistance(φ1, λ1, φ2, λ2);
        var vincenty = this.vincenty(φ1, λ1, φ2, λ2);

        return {
            start: start,
            end: end,
            course: {
                degree: vincenty.route.toDegrees(),
                radians: vincenty.route
            },
            length: {
                m: greatCircleLength,
                km: Math.round(greatCircleLength/1000, 1)
            },
            ellipsoid: {
                distance: {
                    m: Math.round(vincenty.distance),
                    km: Math.round(vincenty.distance/1000 * 100) / 100
                }
            }
        };

    },
    
    getNextCoordinates: function (coords, alpha, accuracy) {
        coords.lon += Math.sin(alpha)*accuracy;
        coords.lat += Math.cos(alpha)*accuracy;

        return coords;
    }

};
